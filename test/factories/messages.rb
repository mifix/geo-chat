# frozen_string_literal: true

FactoryBot.define do
  factory :message do
    content { Faker::HitchhikersGuideToTheGalaxy.quote }
    username { Faker::HitchhikersGuideToTheGalaxy.character }
  end
end
