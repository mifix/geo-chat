
# frozen_string_literal: true

class AuthenticationServiceTest < ActiveSupport::TestCase
  test 'provides a user with session' do
    session = { user_id: FactoryBot.create(:user).id }
    user = AuthenticationService.new(session).user
    assert user.persisted?
    assert_not user.name.empty?
  end

  test 'provides a user with empty session' do
    user = AuthenticationService.new({}).user
    assert user.persisted?
    assert_not user.name.empty?
  end

  test 'provides a user with broken session' do
    user = AuthenticationService.new(user_id: 'foobar').user
    assert user.persisted?
    assert_not user.name.empty?
  end
end
