# frozen_string_literal: true

module ApplicationCable
  # Base Connection ensuring user is authenticated
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = find_verified_user
    end

    private

    def find_verified_user
      User.find cookies.signed[:user_id]
    rescue Mongoid::Errors::DocumentNotFound
      reject_unauthorized_connection
    end
  end
end
