# frozen_string_literal: true

# Chat Messages Channel
class MessagesChannel < ApplicationCable::Channel
  def subscribed
    @group = GroupFinderService.new(current_user, location).group
    stream_for @group
    current_user.update group: @group
    create_join_message
  end

  def unsubscribed
    current_user.update group: nil
    create_leave_message
  end

  private

  def location
    Location.new.tap do |location|
      location.latitude = params[:latitude]
      location.longitude = params[:longitude]
    end
  end

  def create_join_message
    content = '/me joined the group'
    @group.messages.create username: current_user.name, content: content
  end

  def create_leave_message
    content = '/me left the group'
    @group.messages.create username: current_user.name, content: content
  end
end
