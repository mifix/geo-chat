import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Chat from 'components/chat'
import MessagesResource from 'lib/messages_resource'

/**
 * Messages Store connects to messages channel, receives messages and keeps
 * the overall state of messages.
 **/
class MessagesStore extends Component {
  constructor(props) {
    super(props);
    this.send = this.send.bind(this);
    this.receive = this.receive.bind(this);
    this.state = { messages: props.messages, online: false };
    this.api = new MessagesResource();
  }

  componentDidMount() {
    App.cable.subscriptions.create({
        channel: 'MessagesChannel',
        longitude: this.props.coords.longitude,
        latitude: this.props.coords.latitude
      }, {
        connected: () => this.setState({ online: true }),
        disconnected: () => this.setState({ online: false }),
        rejected: () => this.setState({ online: false }),
        received: this.receive
      });
  }

  componentWillUnmount() {
    App.cable.subscriptions.remove({ channel: 'MessagesChannel' });
  }

  receive(message) {
    this.setState({ messages: [...this.state.messages, message] });
  }

  send(content) {
    this.api.create({ content })
      .then(response => console.debug('message create response', response));
  }

  render() {
    return <Chat onSubmit={this.send} {...this.state} />;
  }
}

MessagesStore.defaultProps = { messages: [], coords: {} };

MessagesStore.propTypes = {
  coords: PropTypes.object,
  messages: PropTypes.array,
};

export default MessagesStore;
