import React from 'react'
import ReactDOM from 'react-dom'
import Application from 'lib/application';

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(<Application />, document.getElementById('chat'))
})

