import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Input extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.state = { value: '' };
  }

  submit() {
    this.props.onSubmit(this.state.value.trim());
    this.setState({ value: '' });
  }

  onKeyPress(event) {
    if (event.key !== 'Enter') {
      return;
    }
    this.submit();
  }

  onChange(event) {
    this.setState({ value: event.target.value });
  }

  render() {
    return <input value={this.state.value} className="chat__input" autoFocus
      onKeyPress={this.onKeyPress} onChange={this.onChange} />;
  }
}

Input.propTypes = { onSubmit: PropTypes.func.isRequired }

export default Input;
