import React from 'react'
import PropTypes from 'prop-types'
import ChatInput from 'components/chat/input'
import ChatMessage from 'components/chat/message'

// TODO: use a serializer to convert _id: { $oid } mongoid structure to id
// {messages.map(message => <ChatMessage key={message.id} {...message} />)}
const Chat = ({ online, messages, onSubmit }) => (
  <div className="chat">
    <p className="chat__connection-status">{online ? 'online' : 'offline'}</p>
    <div className="chat__messages">
      {messages.map((message, index) => <ChatMessage key={index} {...message} />)}
    </div>
    <ChatInput onSubmit={onSubmit} />
  </div>
)

Chat.propTypes = {
  online: PropTypes.bool.isRequired,
  messages: PropTypes.array.isRequired,
  onSubmit: PropTypes.func.isRequired
}

export default Chat;
