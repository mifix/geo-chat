# frozen_string_literal: true

# Messages Controller
class MessagesController < ApplicationController
  def create
    @message = messages.new message_params.merge(username: @user.name)
    if @message.save!
      head :created
    else
      head :bad_request
    end
  end

  private

  def message_params
    params.require(:message).permit :content
  end

  def messages
    @user.group.messages
  end
end
