# frozen_string_literal: true

# Find a group by location
class GroupFinderService
  def initialize(user, location)
    @user = user
    @location = location
  end

  def group
    @group ||= (create_new_group? ? new_group : nearest_group) || new_group
    @group
  end

  private

  def create_new_group?
    [*0..9].sample.zero?
  end

  def new_group
    Group.create location: [@location.longitude, @location.latitude]
  end

  def nearest_group
    return random_group unless @location.valid?
    # Group.with_space.near(location: @location.to_qry).first
    random_group # TODO geo location search still fails...
  end

  def random_group
    Group.with_space.sample
  end
end
