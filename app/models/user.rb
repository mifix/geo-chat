# frozen_string_literal: true

# User ready to chat
class User
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String

  belongs_to :group, counter_cache: true, optional: true

  validates :name, presence: true
end
