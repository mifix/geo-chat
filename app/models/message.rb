# frozen_string_literal: true

# A chat message
class Message
  include Mongoid::Document
  include Mongoid::Timestamps

  field :content, type: String
  field :username, type: String

  embedded_in :group

  validates :content, :username, presence: true

  after_create :publish

  private

  def publish
    MessagesChannel.broadcast_to group, self
  end
end
