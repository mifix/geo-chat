# frozen_string_literal: true

# Location somewhere on earth
class Location
  include ActiveModel::AttributeMethods
  include ActiveModel::Validations

  define_attribute_methods :latitude, :longitude
  attr_accessor :latitude, :longitude
  validates :latitude, :longitude, presence: true

  def to_a
    [latitude, longitude]
  end

  def to_qry
    { latitude: latitude.to_f, longitude: longitude.to_f }
  end

  def to_json
    return nil unless valid?
    { type: 'Point', coordinates: to_a }
  end
  alias to_geo_json to_json
end
